const express = require('express')
const router = express.Router()
const cheerio = require('cheerio')
const request = require('request')

router.get('/', function(req, res, next) {
    request({url: "https://ncov.moh.gov.vn/", "rejectUnauthorized": false}, (error, response, html) => {
        if(!error) {
            const $ = cheerio.load(html)
            var data = []
            for (var i = 0; i<8; i++) {
                data.push($('.font24')[i].children[0].data)
            }
            let result = {
                "VN": {
                    "total": data[0],
                    "now": data[1],
                    "recovered": data[2],
                    "deaths": data[3]
                },
                "World": {
                    "total": data[4],
                    "now": data[5],
                    "recovered": data[6],
                    "deaths": data[7]
                }
            }
            res.send(result)
        }
    })
})

module.exports = router